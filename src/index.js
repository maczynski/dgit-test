const Http = new Utils()
let data = null

function showBody(param) {
  document.getElementById('body-content').innerHTML = param.body
}

function showComment(param) {
  document.getElementById('comments-content').innerHTML = ''
  let id = param.id
  let temp = Http.get(`http://jsonplaceholder.typicode.com/posts/${id}/comments`).then(res => {
    res.forEach((item, index) => {
      let div = document.createElement('div')
      let hr = document.createElement('hr')
      let p = document.createElement('p')
      let pBy = document.createElement('p')
      let pEmail = document.createElement('p')
      p.innerHTML = item.body
      pBy.innerHTML = `Name : ${item.name}`
      pEmail.innerHTML = `Email : ${item.email}`
      div.appendChild(p)
      div.appendChild(pBy)
      div.appendChild(pEmail)
      div.appendChild(hr)
      document.getElementById('comments-content').appendChild(div)
    })
  })
}

function search() {
  document.getElementById('body-content').innerHTML = ''
  document.getElementById('comments-content').innerHTML = ''
  let key = document.getElementById('input').value
  let temp = data.filter((item) => {
    if (item.title.toLowerCase().includes(key)) {
      return item
    }
  })
  let tableHeaderRowCount = 1;
  let table = document.getElementById('tableData')
  let rowCount = table.rows.length;
  for (let i = tableHeaderRowCount; i < rowCount; i++) {
    table.deleteRow(tableHeaderRowCount);
  }
  renderTable(temp)
}

function hasWord(text) {
  const key = 'rerum'
  let status = text.toLowerCase().includes(key) ? true : false
  return status
}

function renderTable(collection) {
  let table = document.getElementById('tableData')
  let row = table.insertRow(-1)
  collection.forEach((item, i) => {
    row = table.insertRow(-1);
    let cellNo = row.insertCell(-1);
    cellNo.innerHTML = i + 1;
    if (hasWord(item.body)) {
      cellNo.style.backgroundColor = 'yellow';
    }
    let cellTitle = row.insertCell(-1);
    cellTitle.innerHTML = item.title;

    let buttonsBody = document.createElement('button')
    buttonsBody.className = 'btn btn-primary'
    buttonsBody.innerHTML = 'View'
    buttonsBody.addEventListener('click', function (item) {
      return function () {
        showBody(item)
      }
    }(item))
    let cellButton = row.insertCell(-1);
    cellButton.appendChild(buttonsBody);

    let buttonsComment = document.createElement('button')
    buttonsComment.className = 'btn btn-secondary'
    buttonsComment.innerHTML = 'View'
    buttonsComment.addEventListener('click', function (item) {
      return function () {
        showComment(item)
      }
    }(item))
    let cellButtonComment = row.insertCell(-1);
    cellButtonComment.appendChild(buttonsComment);
  })
}

document.addEventListener("DOMContentLoaded", function (event) {
  document.getElementById('input').addEventListener('keyup', function () {
    search()
  })
  Http.get('http://jsonplaceholder.typicode.com/posts').then(res => {
    data = res
    renderTable(res)
  })
});