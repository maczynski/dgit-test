function Utils() {
  this.get = async (url) => {
    return new Promise((resolve, reject) => {
      fetch(url)
      .then(response => response.json())
      .then(json => resolve(json))
    })
  }
}